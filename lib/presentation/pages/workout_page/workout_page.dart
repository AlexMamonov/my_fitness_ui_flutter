import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:my_fitness_ui_flutter/presentation/common_widgets/calendar_widget.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/workout_page/widgets/workout_card_widget.dart';

import '../../../globals/globals.dart';
import '../../styleguide/colors.dart';
import '../../styleguide/text_style.dart';
import 'widgets/app_bar.dart';

class WorkoutPage extends StatefulWidget {
  WorkoutPage({Key key}) : super(key: key);

  @override
  _WorkoutPageState createState() => _WorkoutPageState();
}

class _WorkoutPageState extends State<WorkoutPage> {
  DateTime _selectedDay;

  List<DateTime> markedDates = [
    DateTime.now().add(Duration(days: 1)),
    DateTime.now(),
  ];
  String _value = 'Семейный';
  // final Map<DateTime, List> _events = {
  //   DateTime(2020, 11, 1): ['New Year\'s Day'],
  //   DateTime(2020, 11, 6): ['Epiphany'],
  //   DateTime(2020, 11, 14): ['Valentine\'s Day'],
  //   DateTime(2020, 11, 21): ['Easter Sunday'],
  //   DateTime(2020, 11, 22): ['Easter Monday'],
  // };

  @override
  void initState() {
    _selectedDay = DateTime.now();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildWorkoutAppBar(context, (String value) {
        setState(() {
          _value = value;
        });
      }, _value),
      backgroundColor: colorDarkGray,
      body: Stack(
        children: [
          Container(),
          Positioned(
            top: 20,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: RotatedBox(
                quarterTurns: 3,
                child: Text(
                  monthLabels[
                      int.parse(new DateFormat('M').format(_selectedDay)) - 1],
                  style: grayColorTextStyle,
                ),
              ),
            ),
          ),
          Positioned(
              left: 5,
              child: Container(
                  padding: const EdgeInsets.only(left: 20),
                  width: deviceWidth,
                  alignment: Alignment.center,
                  child: buildCalendar(
                    markedDates: markedDates,
                    onDateSelected: onSelect,
                    selectedDate: _selectedDay,
                  ))),
          Positioned(
              bottom: 0,
              child: Stack(
                children: [
                  Positioned(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(35.0),
                              topRight: Radius.circular(35.0))),
                      height: deviceHeight > 700 ? 550 : deviceHeight / 1.6,
                      width: deviceWidth,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              WorkoutCardWidget(),
                              WorkoutCardWidget(),
                              WorkoutCardWidget(),
                              WorkoutCardWidget(),
                              SizedBox(
                                height: 100,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Positioned(
                  //     bottom: 100,
                  //     right: 20,
                  //     child: SlideTransition(
                  //         position: _addButtonAnimation, child: AddButton())),
                ],
              )),
        ],
      ),
    );
  }

  onSelect(data) {
    _selectedDay = data;
  }
}
