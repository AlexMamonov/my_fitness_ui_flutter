import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/icons/icons.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

List<DropdownMenuItem<String>> buildDropdownMenuItems(List menu) {
  List<DropdownMenuItem<String>> items = List();
  for (String li in menu) {
    items.add(
      DropdownMenuItem(
        value: li,
        child: SizedBox(
          child: Text(
            li,
            style: mainTextStyle.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
  return items;
}

AppBar buildWorkoutAppBar(
    BuildContext context, Function onChange, String _value) {
  return AppBar(
    elevation: 0,
    title: Padding(
      padding: const EdgeInsets.only(left: 12.0),
      child: DropdownButton(
          dropdownColor: colorDarkGray,
          onChanged: onChange,
          value: _value,
          icon: RotatedBox(
            quarterTurns: 3,
            child: Icon(
              Icons.arrow_left,
              size: 35,
              color: colorPrimaryRed,
            ),
          ),
          underline: SizedBox(
            height: 0,
          ),
          items: buildDropdownMenuItems(<String>['Семейный', 'Одиночный'])),
    ),
    automaticallyImplyLeading: false,
    backgroundColor: colorDarkGray,
    actions: <Widget>[
      Container(
        padding: const EdgeInsets.only(right: 23, top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(
                top: 3.0,
              ),
              width: 30,
              height: 50,
              child: Stack(
                children: [
                  Positioned(top: 10, child: workoutNotificationIcon),
                  Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            color: colorPrimaryRed,
                            borderRadius:
                                BorderRadius.all(Radius.circular(45.0))),
                        width: 18,
                        height: 18,
                        child: Container(
                          child: Text(
                            '2',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ],
        ),
      )
    ],
  );
}
