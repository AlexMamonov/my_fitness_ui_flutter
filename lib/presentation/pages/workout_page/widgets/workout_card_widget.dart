import 'package:flutter/material.dart';

import '../../../styleguide/colors.dart';
import '../../../styleguide/text_style.dart';

class WorkoutCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 35),
                child: Stack(
                  children: [
                    Container(
                        width: 80,
                        height: 50,
                        child: Text("12",
                            style: mainTextStyle.copyWith(fontSize: 45))),
                    Positioned(
                      top: 10,
                      right: 7,
                      child: Text("00",
                          style: mainTextStyle.copyWith(fontSize: 17)),
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    width: 230,
                    height: 90,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: colorLightGray),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text("Урок триатлон/Brick",
                                  style: mainTextStyle.copyWith(fontSize: 17)),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: colorPrimaryRed),
                                  child: Text("По записи",
                                      textAlign: TextAlign.center,
                                      style: grayColorTextStyle.copyWith(
                                          fontSize: 12, color: Colors.white)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
