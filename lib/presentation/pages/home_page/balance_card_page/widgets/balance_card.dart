import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/globals/globals.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/images/images.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class BalanceCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: Container(
        width: 295,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                blurRadius: 20,
                spreadRadius: 1,
                color: colorBalanceCardShadowDark,
                offset: Offset(20.0, 20.0)),
            BoxShadow(
                blurRadius: 20,
                spreadRadius: 1,
                color: colorBalanceCardShadowLight,
                offset: Offset(-20.0, -20.0))
          ],
        ),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(37.0),
                      bottomLeft: Radius.circular(37.0)),
                  color: colorPrimaryRed,
                  border: Border.all(
                      width: 1.0,
                      color: const Color(0xFFFFFFFF).withOpacity(0.4))),
              width: 300,
              height: deviceHeight / 10 * 7,
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(37.0),
                    bottomLeft: Radius.circular(37.0)),
                color: colorPrimaryRed,
                backgroundBlendMode: BlendMode.softLight,
                // color: colorPrimaryRed,
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    const Color(0xFF000000).withOpacity(0.4),
                    const Color(0xFFFFFFFF).withOpacity(0.4),
                  ],
                ),
              ),
              width: 300,
              height: deviceHeight / 10 * 7,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(32.0, 40.0, 0.0, 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "52 000 P",
                              style: primaryTextStyleWhite,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 30.0),
                              child: InkWell(
                                onTap: () {},
                                child: Text(
                                  "+",
                                  style: primaryTextStyleWhite.copyWith(
                                      fontStyle: FontStyle.normal,
                                      fontSize: 40,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "2000 бонусов",
                            style: whiteTextStyleSmall,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Center(child: Image.asset(barcode_image)),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
