import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/balance_card_page/widgets/balance_card.dart';

class BalanceCardPage extends StatefulWidget {
  @override
  _BalanceCardPageState createState() => _BalanceCardPageState();
}

class _BalanceCardPageState extends State<BalanceCardPage> {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: BalanceCard(),
        ));
  }
}
