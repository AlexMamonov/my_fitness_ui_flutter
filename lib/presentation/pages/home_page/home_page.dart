import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/widgets/app_bar.dart';
import '../home_page/balance_card_page/widgets/balance_card.dart';
import '../home_page/clip_cards_page/clip_cards_page.dart';
import '../home_page/contract_page/contact_page.dart';
import '../home_page/guest_ticked_page/guest_ticked_page.dart';
import '../home_page/locker_rental_page/locker_rental_page.dart';
import '../home_page/widgets/add_button.dart';
import '../home_page/widgets/rotated_menu.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  PageController _pageController;
  double changeInY;
  int currentPage = 0;
  Animation<Offset> _addButtonAnimation;
  AnimationController _addButtonController;

  @override
  void initState() {
    _pageController = new PageController(initialPage: 0);
    _addButtonController = AnimationController(
      duration: Duration(milliseconds: 1500),
      vsync: this,
    );

    _addButtonAnimation = Tween<Offset>(
      begin: const Offset(3.5, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
        parent: _addButtonController,
        curve: Interval(
          0.75,
          1.0,
          curve: Curves.easeInOut,
        )));
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _addButtonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    if (currentPage >= 1) {
      _addButtonController.forward();
    } else
      _addButtonController.reverse();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: fintessAppBar,
      body: Stack(
        children: [
          GestureDetector(
            onVerticalDragUpdate: (DragUpdateDetails details) {
              changeInY = -details.delta.dy;
            },
            onVerticalDragEnd: (DragEndDetails details) {
              setState(() {
                if (changeInY > 1) {
                  _pageController.nextPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn);
                }
                if (changeInY < -1) {
                  _pageController.previousPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn);
                }
              });
            },
            child: Container(
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowGlow();
                  return false;
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 50),
                  child: PageView(
                    onPageChanged: (index) {
                      setState(() {
                        currentPage = index;
                      });
                    },
                    scrollDirection: Axis.vertical,
                    controller: _pageController,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      BalanceCard(),
                      ContactCardPage(
                        currentPage: currentPage,
                      ),
                      ClipCardsPage(),
                      LockerRentalPage(),
                      GuestTicketPage(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 100),
              child: Stack(
                children: [
                  RotatedMenu(
                    pageController: _pageController,
                    currentPage: currentPage,
                  ),
                  currentPage > 2
                      ? Positioned(
                          top: 0,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 15,
                                      spreadRadius: 35,
                                      color: Colors.white,
                                      offset: Offset(0.0, 0.0))
                                ],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(45.0))),
                            width: 20,
                            height: 20,
                          ),
                        )
                      : Positioned(
                          bottom: 0,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 15,
                                      spreadRadius: 20,
                                      color: Colors.white,
                                      offset: Offset(0.0, 0.0))
                                ],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(45.0))),
                            width: 20,
                            height: 20,
                          ),
                        ),
                ],
              )),
          Positioned(
              bottom: 100,
              right: 20,
              child: SlideTransition(
                  position: _addButtonAnimation, child: AddButton())),
        ],
      ),
    );
  }
}
