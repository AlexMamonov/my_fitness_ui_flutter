import 'package:flutter/material.dart';

import 'widgets/quarter_widget.dart';
import 'widgets/tariff_widget.dart';

class ContactCardPage extends StatefulWidget {
  final int currentPage;
  ContactCardPage({Key key, this.currentPage}) : super(key: key);

  @override
  _ContactCardState createState() => _ContactCardState();
}

class _ContactCardState extends State<ContactCardPage>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _quarterAnimation;
  Animation<Offset> _tariffAnimation;
  int currentPage;
  @override
  void initState() {
    currentPage = widget.currentPage;
    _controller = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    )..forward();

    _quarterAnimation = Tween<Offset>(
      begin: const Offset(0.0, 3.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOutCirc,
    ));

    _tariffAnimation = Tween<Offset>(
      begin: const Offset(0.0, 3.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.25,
          1.0,
          curve: Curves.easeInOutCirc,
        )));

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 30),
      child: Stack(
        children: [
          if (currentPage == 0)
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SlideTransition(
                    position: _quarterAnimation,
                    child: QuarterWidget(),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SlideTransition(
                    child: TariffWidget(),
                    position: _tariffAnimation,
                  ),
                ],
              ),
            ),
          if (currentPage != 0)
            Center(
              child: Column(
                children: [
                  QuarterWidget(),
                  SizedBox(
                    height: 30,
                  ),
                  TariffWidget(),
                ],
              ),
            )
        ],
      ),
    );
  }
}
