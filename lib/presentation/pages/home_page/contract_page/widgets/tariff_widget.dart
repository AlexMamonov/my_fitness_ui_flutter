import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class TariffWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 260,
        height: 210,
        decoration: BoxDecoration(
          border: Border.all(
              width: 1.0, color: const Color(0xFFFFFFFF).withOpacity(0.16)),
          boxShadow: [
            BoxShadow(
                blurRadius: 20,
                spreadRadius: 1,
                color: colorBalanceCardShadowDark,
                offset: Offset(20.0, 20.0)),
          ],
        ),
        child: Card(
          margin: EdgeInsets.only(left: 10),
          color: Colors.white,
          clipBehavior: Clip.hardEdge,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
            Radius.circular(30.0),
          )),
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: colorOrange,
                    ),
                    child: IconButton(
                      icon: Icon(
                        Icons.watch_later_outlined,
                        size: 30,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                ),
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Митино",
                        style: mainTextStyle,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Ожидает оплаты",
                        textAlign: TextAlign.start,
                        style: paymentWaitingTextStyle,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
