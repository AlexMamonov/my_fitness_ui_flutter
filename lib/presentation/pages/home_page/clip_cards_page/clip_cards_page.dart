import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/clip_cards_page/widgets/empty_clip_card_widget.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';

import '../../../../globals/globals.dart';
import 'widgets/clip_card_widget.dart';

class ClipCardsPage extends StatefulWidget {
  @override
  _ClipCardsPageState createState() => _ClipCardsPageState();
}

class _ClipCardsPageState extends State<ClipCardsPage>
    with TickerProviderStateMixin {
  AnimationController _clipCardController;
  Animation<double> _clipCardanimation;
  Animation<Offset> _offsetClipCardAnimation;

  @override
  void initState() {
    _clipCardController = AnimationController(
      duration: Duration(milliseconds: 1200),
      vsync: this,
    )..forward();
    super.initState();
  }

  @override
  void dispose() {
    _clipCardController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool deviceMoreThan5Inch = deviceHeight > 700;
    int currentIndex = 5;
    return Container(
      padding: EdgeInsets.only(
          bottom:
              deviceMoreThan5Inch ? deviceHeight / 10 * 3 : deviceHeight / 7,
          right: deviceMoreThan5Inch ? 20 : 40),
      child: Stack(
        children: [
          Positioned(
            top: deviceMoreThan5Inch ? 40 : 50,
            left: deviceMoreThan5Inch ? 20 : 40,
            child: animateCardWidget(
              3,
              Container(
                width: 200,
                height: 418,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 10,
                          spreadRadius: 0.5,
                          color: colorPrimaryRed.withOpacity(0.4),
                          offset: Offset(0.0, 0.0)),
                    ]),
              ),
            ),
          ),
          Positioned(
            child: Swiper(
              onIndexChanged: (int index) {
                currentIndex = index;
              },
              itemBuilder: (BuildContext context, int index) {
                return animateCardWidget(
                  index,
                  index + 2 == currentIndex
                      ? ClipCardWidget()
                      : index - 3 == currentIndex
                          ? ClipCardWidget()
                          : EmptyClipCardWidget(),
                );
              },
              autoplay: false,
              itemCount: 5,
              viewportFraction: 30.0,
              itemWidth: 228.0,
              itemHeight: deviceHeight > 700 ? 420.0 : 400,
              layout: SwiperLayout.CUSTOM,
              customLayoutOption:
                  CustomLayoutOption(startIndex: 1, stateCount: 4).addOpacity([
                0.4,
                1.0,
                1.0,
                1.0,
              ]).addTranslate([
                Offset(70.0, 0.0),
                Offset(20.0, 0.0),
                Offset(-20.0, 0.0),
                Offset(600.0, 0.0),
              ]).addScale([0.5, 0.8, 1.0, 1.0], Alignment.center),
              loop: true,
            ),
          ),
        ],
      ),
    );
  }

  Widget animateCardWidget(int index, Widget widget) {
    _offsetClipCardAnimation = Tween<Offset>(
      begin: const Offset(0.5, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _clipCardController,
      curve: Interval(0.5 / index, 1.0,
          curve: Interval(0.5 / index, 1.0,
              curve: Interval(0.8 / (index), 1.0, curve: Curves.easeOut))),
    ));
    _clipCardanimation = CurvedAnimation(
        parent: _clipCardController,
        curve: Interval(0.5 / index, 1.0,
            curve: Interval(0.5 / index, 1.0,
                curve: Interval(0.8 / (index), 1.0, curve: Curves.easeOut))));
    return SlideTransition(
        position: _offsetClipCardAnimation,
        child: ScaleTransition(scale: _clipCardanimation, child: widget));
  }
}
