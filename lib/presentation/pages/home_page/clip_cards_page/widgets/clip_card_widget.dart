import 'dart:math';

import 'package:flutter/material.dart';

import '../../../../styleguide/colors.dart';
import '../../../../styleguide/text_style.dart';

class ClipCardWidget extends StatelessWidget {
  ClipCardWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(left: 10),
      color: colorPrimaryRed,
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
        Radius.circular(30.0),
      )),
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0, left: 30.00),
        child: Stack(children: [
          Column(
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Игровые Группы на выбор",
                    style: primaryTextStyleWhite.copyWith(fontSize: 30),
                  )),
              SizedBox(
                height: 30,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("до 12 июня",
                    style: whiteTextStyleSmall.copyWith(
                      color: Colors.white.withOpacity(0.5),
                    )),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: 300,
                  height: 100,
                  child: Stack(
                    children: [
                      Text(
                        "3/10",
                        style: primaryTextStyleWhite,
                      ),
                      Positioned(
                        top: 20,
                        child: Row(
                          children: [
                            Text(
                              "...",
                              textAlign: TextAlign.start,
                              style: primaryTextStyleWhite.copyWith(
                                  letterSpacing: 2.0),
                            ),
                            Text(
                              ".......",
                              textAlign: TextAlign.start,
                              style: primaryTextStyleWhite.copyWith(
                                  color: colorDarkRed, letterSpacing: 2.0),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              width: 100,
              height: 60,
              decoration: BoxDecoration(
                  color: colorDarkGray,
                  borderRadius:
                      BorderRadius.only(topLeft: Radius.circular(30))),
              child: IconButton(
                onPressed: () {},
                iconSize: 32,
                icon: Transform(
                    alignment: Alignment.center,
                    transform: Matrix4.rotationY(pi),
                    child: Icon(Icons.arrow_back)),
                color: Colors.white,
              ),
            ),
          )
        ]),
      ),
    );
  }
}
