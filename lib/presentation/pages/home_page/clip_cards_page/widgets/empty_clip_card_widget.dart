import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';

class EmptyClipCardWidget extends StatelessWidget {
  EmptyClipCardWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(left: 10),
      color: colorPinkClipCard,
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
        Radius.circular(30.0),
      )),
      child: Container(
        width: 228,
        height: 420,
      ),
    );
  }
}
