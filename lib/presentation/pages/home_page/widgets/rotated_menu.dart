import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/widgets/rotated_menu_item.dart';

class RotatedMenu extends StatefulWidget {
  final PageController pageController;
  final int currentPage;

  RotatedMenu({Key key, this.pageController, this.currentPage})
      : super(key: key);
  @override
  _RotatedMenuState createState() => _RotatedMenuState();
}

class _RotatedMenuState extends State<RotatedMenu> {
  ScrollController scrollController;

  @override
  void initState() {
    scrollController = new ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PageController pageController = widget.pageController;
    int currentPage = widget.currentPage;

    if (scrollController.hasClients) {
      if (currentPage > 2) {
        scrollController.animateTo(scrollController.position.maxScrollExtent,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 500));
      } else {
        scrollController.animateTo(scrollController.position.minScrollExtent,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 500));
      }
    }
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) {
        overscroll.disallowGlow();
        return false;
      },
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RotatedMenuItem(
                  menuItemTitle: 'Баланс',
                  pageController: pageController,
                  pageNumber: 0,
                  currentPage: currentPage,
                ),
                RotatedMenuItem(
                  menuItemTitle: 'Мои контракты',
                  pageController: pageController,
                  pageNumber: 1,
                  currentPage: currentPage,
                ),
                RotatedMenuItem(
                  menuItemTitle: 'Мои клип-карты',
                  pageController: pageController,
                  pageNumber: 2,
                  currentPage: currentPage,
                ),
                RotatedMenuItem(
                  menuItemTitle: 'Аренда шкафчика',
                  pageController: pageController,
                  pageNumber: 3,
                  currentPage: currentPage,
                ),
                RotatedMenuItem(
                  menuItemTitle: 'Гостевой визит',
                  pageController: pageController,
                  pageNumber: 4,
                  currentPage: currentPage,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
