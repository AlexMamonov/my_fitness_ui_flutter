import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/icons/icons.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

var fintessAppBar = AppBar(
  elevation: 0,
  title: Padding(
    padding: const EdgeInsets.only(left: 12.0),
    child: Text(
      "#MYFITNESS",
      style: mainTextStyle,
      textAlign: TextAlign.center,
    ),
  ),
  automaticallyImplyLeading: false,
  backgroundColor: Colors.white,
  actions: <Widget>[
    Container(
      padding: const EdgeInsets.only(right: 32, top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                  top: 3.0,
                ),
                child: Stack(
                  children: [
                    notificationIcon,
                    Positioned(
                        right: 2,
                        top: 4,
                        child: Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 0,
                                    spreadRadius: 3,
                                    color: Colors.white,
                                    offset: Offset(0.0, 0.0))
                              ],
                              color: colorPrimaryRed,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(45.0))),
                          width: 8,
                          height: 8,
                        )),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    )
  ],
);
