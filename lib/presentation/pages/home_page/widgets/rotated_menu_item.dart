import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class RotatedMenuItem extends StatefulWidget {
  final String menuItemTitle;
  final int currentPage;
  final PageController pageController;
  final int pageNumber;

  RotatedMenuItem({
    Key key,
    this.menuItemTitle,
    this.currentPage,
    this.pageController,
    this.pageNumber,
  }) : super(key: key);

  @override
  _RotatedMenuItemState createState() => _RotatedMenuItemState();
}

class _RotatedMenuItemState extends State<RotatedMenuItem> {
  @override
  Widget build(BuildContext context) {
    var menuItemTitle = widget.menuItemTitle;
    return RotatedBox(
      quarterTurns: 3,
      child: FlatButton(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        onPressed: () {
          widget.pageController.animateToPage(widget.pageNumber,
              duration: Duration(milliseconds: 500), curve: Curves.easeIn);
        },
        child: AnimatedDefaultTextStyle(
          duration: Duration(milliseconds: 200),
          style: widget.currentPage == widget.pageNumber
              ? selectedColorTextStyle
              : grayColorTextStyle,
          child: Text(
            menuItemTitle,
          ),
        ),
      ),
    );
  }
}
