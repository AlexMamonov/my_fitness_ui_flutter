import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';

class AddButton extends StatelessWidget {
  const AddButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              blurRadius: 20,
              spreadRadius: 1,
              color: colorBalanceCardShadowDark,
              offset: Offset(20.0, 20.0))
        ],
      ),
      child: FloatingActionButton(
        onPressed: () {},
        backgroundColor: colorPrimaryRed,
        child: Icon(Icons.add),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      ),
    );
  }
}
