import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class PlannedCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Container(
        width: 260,
        height: 135,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: colorLightGray),
        padding: const EdgeInsets.fromLTRB(20, 10, 0, 30),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("12.06", style: mainTextStyle.copyWith(fontSize: 45)),
                  Text("10.00 - 11.00\n Квартал",
                      style: grayLabelColorTextStyle.copyWith(fontSize: 13)),
                  SizedBox(
                    width: 10,
                  )
                ],
              ),
            ),
            Positioned(
                bottom: 0,
                child: Text("Дарья Кулышева",
                    style: mainTextStyle.copyWith(fontSize: 17))),
          ],
        ),
      ),
    );
  }
}
