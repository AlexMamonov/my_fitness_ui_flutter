import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';
import 'dart:math';

class GuestVisitWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: colorPrimaryRed,
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
                blurRadius: 20,
                spreadRadius: 1,
                color: Colors.grey[350],
                offset: Offset(20.0, 20.0))
          ]),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 20, 0, 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: [
                Text(
                  "6",
                  style: primaryTextStyleWhite.copyWith(fontSize: 45),
                ),
                SizedBox(
                  width: 13,
                ),
                Text(
                  "Гостевых\nвизитов",
                  style: primaryTextStyleWhite.copyWith(fontSize: 17),
                ),
              ],
            ),
            Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white),
                child: Transform(
                  alignment: Alignment.center,
                  transform: Matrix4.rotationY(pi),
                  child: IconButton(
                      icon: Icon(
                        Icons.reply,
                        color: colorPrimaryRed,
                      ),
                      onPressed: () {}),
                ))
          ],
        ),
      ),
    );
  }
}
