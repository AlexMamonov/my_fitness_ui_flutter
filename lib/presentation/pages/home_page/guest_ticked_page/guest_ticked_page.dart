import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/guest_ticked_page/widgets/guest_visits_widget.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/guest_ticked_page/widgets/planned_card_widget.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class GuestTicketPage extends StatefulWidget {
  GuestTicketPage({Key key}) : super(key: key);

  @override
  _GuestTicketPageState createState() => _GuestTicketPageState();
}

class _GuestTicketPageState extends State<GuestTicketPage>
    with TickerProviderStateMixin {
  AnimationController _guestController;
  AnimationController _plannedController;
  Animation<Offset> _guestVisitAnimation;
  Animation<Offset> _plannedVisitAnimation;
  int currentPage;
  @override
  void initState() {
    _guestController = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    )..forward();

    _plannedController = AnimationController(
      duration: Duration(milliseconds: 1200),
      vsync: this,
    )..forward();

    _guestVisitAnimation = Tween<Offset>(
      begin: const Offset(3.0, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _guestController,
      curve: Curves.easeInOutCirc,
    ));

    super.initState();
  }

  @override
  void dispose() {
    _guestController.dispose();
    _plannedController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 260,
      padding: const EdgeInsets.only(left: 20, top: 20),
      child: Padding(
        padding: const EdgeInsets.only(right: 32, bottom: 0),
        child: Column(
          children: [
            SlideTransition(
              position: _guestVisitAnimation,
              child: GuestVisitWidget(),
            ),
            SizedBox(
              height: 52,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text("Запланированно",
                  style: grayColorTextStyle.copyWith(
                      fontStyle: FontStyle.italic,
                      color: Color(0xFFC1BECB),
                      fontWeight: FontWeight.bold,
                      fontSize: 17)),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: 2,
                itemBuilder: (BuildContext context, int index) {
                  _plannedVisitAnimation = Tween<Offset>(
                    begin: const Offset(5.0, 0.0),
                    end: const Offset(0.0, 0.0),
                  ).animate(CurvedAnimation(
                      parent: _plannedController,
                      curve: Interval(0.2 * index, 1.0,
                          curve: Curves.easeInOutCirc)));
                  return SlideTransition(
                    child: PlannedCardWidget(),
                    position: _plannedVisitAnimation,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
