import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';

import '../../../../globals/globals.dart';
import 'widgets/locker_rental_widget.dart';

class LockerRentalPage extends StatefulWidget {
  LockerRentalPage({Key key}) : super(key: key);

  @override
  _LockerRentalPageState createState() => _LockerRentalPageState();
}

class _LockerRentalPageState extends State<LockerRentalPage>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _lockerAnimation;
  double changeInX;

  ScrollController _scrollController;
  @override
  void initState() {
    _scrollController = new ScrollController();
    _controller = new AnimationController(
      duration: Duration(seconds: 2),
      vsync: this,
    )..forward();

    _lockerAnimation = new Tween<Offset>(
      begin: const Offset(1.0, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List<Widget> lockers = [
    LockerRentalWidget(
      someNumber: 23,
      isMale: true,
    ),
    LockerRentalWidget(
      someNumber: 118,
      isMale: false,
    ),
    LockerRentalWidget(
      someNumber: 142,
      isMale: false,
    ),
    LockerRentalWidget(
      someNumber: 23,
      isMale: true,
    ),
    LockerRentalWidget(
      someNumber: 142,
      isMale: false,
    ),
    LockerRentalWidget(
      someNumber: 118,
      isMale: true,
    ),
    LockerRentalWidget(
      someNumber: 142,
      isMale: false,
    ),
    LockerRentalWidget(
      someNumber: 23,
      isMale: true,
    ),
    LockerRentalWidget(
      someNumber: 142,
      isMale: false,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 25),
      child: GestureDetector(
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          changeInX = -details.delta.dx;
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          setState(() {
            if (changeInX > 1 && _scrollController.offset < 1020.0) {
              _scrollController.animateTo(_scrollController.offset + 170.0,
                  curve: Curves.easeIn, duration: Duration(milliseconds: 200));
            }
            if (changeInX < -1) {
              _scrollController.animateTo(_scrollController.offset - 170.0,
                  curve: Curves.easeIn, duration: Duration(milliseconds: 200));
            }
          });
        },
        child: LiveList.options(
          controller: _scrollController,
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          padding: EdgeInsets.only(
              bottom: deviceHeight / 10 * 3, top: 20, right: 100),
          itemCount: lockers.length,
          options: LiveOptions(
            showItemInterval: Duration(milliseconds: 300),
            delay: Duration(milliseconds: 100),
            showItemDuration: Duration(milliseconds: 300),
          ),
          addRepaintBoundaries: true,
          scrollDirection: Axis.horizontal,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, item, animation) {
            return Container(
              width: 160,
              height: 420,
              padding: const EdgeInsets.only(right: 10),
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(2.0, 0.0),
                  end: const Offset(0.0, 0.0),
                ).animate(animation),
                child: SlideTransition(
                    position: _lockerAnimation, child: lockers[item]),
              ),
            );
          },
        ),
      ),
    );
  }
}
