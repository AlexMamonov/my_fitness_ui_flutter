import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/icons/icons.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class LockerRentalWidget extends StatelessWidget {
  final int someNumber;
  final bool isMale;
  const LockerRentalWidget({Key key, this.someNumber, this.isMale})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 150,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                blurRadius: 15,
                spreadRadius: 1,
                color: colorBalanceCardShadowDark.withOpacity(0.5),
                offset: Offset(20.0, 20.0))
          ],
        ),
        child: Card(
          margin: EdgeInsets.only(left: 10),
          color: Colors.white,
          clipBehavior: Clip.hardEdge,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
            Radius.circular(30.0),
          )),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    someNumber.toString(),
                    style: mainTextStyle.copyWith(fontSize: 40),
                  ),
                ),
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Варшава",
                        style: lockerNameTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "До 12 дек 2020",
                        textAlign: TextAlign.start,
                        style: grayColorTextStyle.copyWith(
                            fontSize: 13,
                            fontStyle: FontStyle.normal,
                            color: colorLabelGray),
                      ),
                    ),
                  ],
                ),
                Container(
                    width: 90,
                    height: 90,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: isMale ? colorDarkGray : colorPrimaryRed,
                    ),
                    child: Container(
                        padding: const EdgeInsets.all(15),
                        child: isMale ? maleIcon : femaleIcon)),
              ],
            ),
          ),
        ));
  }
}
