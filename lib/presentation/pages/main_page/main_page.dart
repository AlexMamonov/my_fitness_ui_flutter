import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/home_page.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/schedule_page/schedule_page.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/workout_page/workout_page.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import '../../../globals/globals.dart';
import 'widgets/bottom_navbar.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  PageController _pageController;
  int currentPage = 0;

  @override
  void initState() {
    _pageController = new PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    return Material(
      color: currentPage == 0 ? Colors.white : colorDarkGray,
      child: Stack(
        children: [
          Container(
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowGlow();
                return false;
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: PageView(
                  onPageChanged: (index) {
                    setState(() {
                      currentPage = index;
                    });
                  },
                  scrollDirection: Axis.horizontal,
                  controller: _pageController,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    HomePage(),
                    SchedulePage(),
                    WorkoutPage(),
                    Container(),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
              child: BottomNavBar(
            currentPage: currentPage,
            pageController: _pageController,
          ))
        ],
      ),
    );
  }
}
