import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class BottomBarItem extends StatefulWidget {
  final String text;
  final Widget icon;
  final Widget selectedIcon;
  final int currentPage;
  final PageController pageController;
  final int pageNumber;
  BottomBarItem(
      {Key key,
      this.text,
      this.icon,
      this.currentPage,
      this.pageNumber,
      this.selectedIcon,
      this.pageController})
      : super(key: key);

  @override
  _BottomBarItemState createState() => _BottomBarItemState();
}

class _BottomBarItemState extends State<BottomBarItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.pageController.jumpToPage(
          widget.pageNumber,
        );
      },
      child: Container(
        width: 80,
        height: 50,
        child: widget.currentPage != widget.pageNumber
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(top: widget.pageNumber == 3 ? 3 : 0),
                    child: Column(
                      children: [
                        widget.icon,
                      ],
                    ),
                  ),
                  if (widget.pageNumber == 3)
                    SizedBox(
                      height: 8,
                    ),
                  Text(
                    widget.text,
                    style: menuTextStyleSmall,
                  )
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 80,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(30.0)),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        child: widget.selectedIcon,
                        width: 20,
                        height: 20,
                      )),
                ],
              ),
      ),
    );
  }
}
