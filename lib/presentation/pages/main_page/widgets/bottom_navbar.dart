import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/globals/globals.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/main_page/widgets/bottom_bar_item.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/icons/icons.dart';

class BottomNavBar extends StatefulWidget {
  final int currentPage;
  final PageController pageController;
  BottomNavBar({Key key, this.currentPage, this.pageController})
      : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  @override
  Widget build(BuildContext context) {
    int currentPage = widget.currentPage;
    PageController pageController = widget.pageController;
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
        child: Container(
          width: deviceWidth,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  width: 1.0, color: const Color(0xFFFFFFFF).withOpacity(0.16)),
              boxShadow: [
                BoxShadow(
                    blurRadius: 20,
                    spreadRadius: 1,
                    color: colorBalanceCardShadowDark,
                    offset: Offset(20.0, 20.0)),
                // BoxShadow(
                //     blurRadius: 20,
                //     spreadRadius: 1,
                //     color: colorBalanceCardShadowLight,
                //     offset: Offset(-30.0, -30.0))
              ],
              borderRadius: BorderRadius.circular(40)),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                BottomBarItem(
                  text: "Главная",
                  icon: homeIcon,
                  selectedIcon: selectedHomeIcon,
                  currentPage: currentPage,
                  pageController: pageController,
                  pageNumber: 0,
                ),
                BottomBarItem(
                  text: "Активность",
                  icon: workoutIcon,
                  selectedIcon: selectedWorkoutIcon,
                  currentPage: currentPage,
                  pageController: pageController,
                  pageNumber: 1,
                ),
                BottomBarItem(
                  text: "Расписание",
                  icon: scheduleIcon,
                  selectedIcon: selectedScheduleIcon,
                  currentPage: currentPage,
                  pageController: pageController,
                  pageNumber: 2,
                ),
                BottomBarItem(
                  text: "Ещё",
                  icon: moreIcon,
                  selectedIcon: moreIcon,
                  currentPage: currentPage,
                  pageController: pageController,
                  pageNumber: 3,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
