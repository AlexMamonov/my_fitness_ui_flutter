import 'package:flutter/material.dart';

import '../../../styleguide/colors.dart';
import '../../../styleguide/text_style.dart';

class ActivityCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Container(
        width: 300,
        height: 136,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(32), color: colorLightGray),
        padding: const EdgeInsets.fromLTRB(20, 20, 0, 10),
        child: Row(
          children: [
            Column(
              // mainAxisSize: MainAxisSize.max,
              children: [
                Text("12:00", style: mainTextStyle.copyWith(fontSize: 30)),
                Text("13:00", style: mainTextStyle.copyWith(fontSize: 30)),
              ],
            ),
            SizedBox(
              width: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 150,
                    child: Text("Тренировка по боевым искусствам",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: mainTextStyle.copyWith(fontSize: 18)),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text("Семейный",
                      style: grayColorTextStyle.copyWith(fontSize: 13)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
