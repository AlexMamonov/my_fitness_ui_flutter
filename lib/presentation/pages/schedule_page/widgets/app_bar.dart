import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/icons/icons.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

AppBar scheduleAppBar = AppBar(
  elevation: 0,
  title: Padding(
    padding: const EdgeInsets.only(left: 12.0),
    child: Text(
      "Моя активность",
      style: mainTextStyle.copyWith(color: Colors.white),
      textAlign: TextAlign.center,
    ),
  ),
  automaticallyImplyLeading: false,
  backgroundColor: colorDarkGray,
  actions: <Widget>[
    Container(
      padding: const EdgeInsets.only(right: 23, top: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
              top: 3.0,
            ),
            width: 30,
            height: 50,
            child: Stack(
              children: [
                Positioned(top: 10, child: scheduleNotificationIcon),
              ],
            ),
          ),
        ],
      ),
    )
  ],
);
