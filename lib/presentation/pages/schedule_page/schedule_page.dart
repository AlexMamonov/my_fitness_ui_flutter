import 'package:flutter/material.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:my_fitness_ui_flutter/presentation/common_widgets/calendar_widget.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/home_page/widgets/add_button.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/schedule_page/widgets/schedule_card_widget.dart';

import '../../../globals/globals.dart';
import '../../styleguide/colors.dart';
import '../../styleguide/text_style.dart';
import 'widgets/app_bar.dart';

class SchedulePage extends StatefulWidget {
  SchedulePage({Key key}) : super(key: key);

  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage>
    with SingleTickerProviderStateMixin {
  DateTime _selectedDay;
  DateTime selectedDate = DateTime.now();
  Animation<Offset> _addButtonAnimation;
  AnimationController _addButtonController;
  List<DateTime> markedDates = [
    DateTime.now().add(Duration(days: 1)),
    DateTime.now(),
  ];

  @override
  void initState() {
    _addButtonController = AnimationController(
      duration: Duration(milliseconds: 750),
      vsync: this,
    )..forward();

    _addButtonAnimation = Tween<Offset>(
      begin: const Offset(3.0, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
        parent: _addButtonController,
        curve: Interval(
          0.0,
          1.0,
          curve: Curves.easeInOut,
        )));
    _selectedDay = DateTime.now();

    super.initState();
  }

  @override
  void dispose() {
    _addButtonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: scheduleAppBar,
      backgroundColor: colorDarkGray,
      body: Stack(
        children: [
          // Container(),
          Positioned(
            top: 20,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10),
              child: RotatedBox(
                quarterTurns: 3,
                child: Center(
                  child: Text(
                    monthLabels[
                        int.parse(new DateFormat('M').format(_selectedDay)) -
                            1],
                    style: grayColorTextStyle,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              left: 5,
              child: Container(
                padding: const EdgeInsets.only(left: 20),
                width: deviceWidth,
                alignment: Alignment.center,
                child: buildCalendar(
                  markedDates: markedDates,
                  onDateSelected: onSelect,
                  selectedDate: _selectedDay,
                ),
              )),
          Positioned(
              bottom: 0,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(35.0),
                        topRight: Radius.circular(35.0))),
                height: deviceHeight > 700 ? 550 : deviceHeight / 1.6,
                width: deviceWidth,
                child: Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        ActivityCardWidget(),
                        ActivityCardWidget(),
                        ActivityCardWidget(),
                        ActivityCardWidget(),
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ),
                  ),
                ),
              )),
          Positioned(
              bottom: 100,
              right: 20,
              child: SlideTransition(
                  position: _addButtonAnimation, child: AddButton())),
        ],
      ),
    );
  }

  onSelect(data) {
    _selectedDay = data;
  }
}
