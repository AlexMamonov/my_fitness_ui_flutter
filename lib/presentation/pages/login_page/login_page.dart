import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/globals/globals.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;
    bool isDeviceMore5Inch = deviceHeight > 700;
    return Scaffold(
      backgroundColor: colorPrimaryRed,
      body: Padding(
        padding: const EdgeInsets.only(top: 50.0),
        child: Stack(
          children: [
            Container(),
            RotatedBox(
              quarterTurns: 3,
              child: Text(
                '#MYFITNESS',
                style: primaryTextStyleWhite,
              ),
            ),
            Positioned(
              right: 20,
              top: 0,
              child: Text(
                'Регистрация',
                style: whiteTextStyleSmall.copyWith(
                    fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
              ),
            ),
            Positioned(
                bottom: 0,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(35.0),
                          topRight: Radius.circular(35.0))),
                  height: isDeviceMore5Inch ? 420 : deviceHeight / 1.77,
                  width: deviceWidth,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          child: Column(
                            children: [
                              TextField(
                                style: mainTextStyle,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  counterStyle: mainTextStyle,
                                  labelText: 'Логин или номер карты',
                                  labelStyle: loginPageLabelStyle,
                                ),
                              ),
                              SizedBox(
                                height: isDeviceMore5Inch ? 28 : 10,
                              ),
                              TextField(
                                style:
                                    mainTextStyle.copyWith(letterSpacing: 10),
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.visiblePassword,
                                autocorrect: false,
                                obscuringCharacter: '*',
                                obscureText: true,
                                decoration: InputDecoration(
                                    focusColor: Colors.grey,
                                    fillColor: Colors.green,
                                    border: InputBorder.none,
                                    counterStyle: mainTextStyle.copyWith(
                                        letterSpacing: 2),
                                    labelText: 'Пароль',
                                    labelStyle: loginPageLabelStyle),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 45,
                        ),
                        FlatButton(
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  '/', (Route<dynamic> route) => false);
                            },
                            child: Container(
                              height: deviceHeight / 10,
                              child: Stack(
                                alignment: Alignment.topCenter,
                                children: [
                                  Positioned(
                                    top: 10,
                                    child: Container(
                                      width: 246,
                                      height: deviceHeight / 14,
                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                                blurRadius: 10,
                                                spreadRadius: 0.5,
                                                color: colorPrimaryRed
                                                    .withOpacity(0.3),
                                                offset: Offset(0.0, 1.0))
                                          ],
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(45))),
                                    ),
                                  ),
                                  Stack(
                                    children: [
                                      Container(
                                        width: 310,
                                        height: deviceHeight / 14,
                                        decoration: BoxDecoration(
                                            color: colorPrimaryRed,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(45))),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'Войти',
                                            style: whiteTextStyleSmall.copyWith(
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 310,
                                        height: deviceHeight / 14,
                                        decoration: BoxDecoration(
                                            backgroundBlendMode:
                                                BlendMode.softLight,
                                            // color: colorPrimaryRed,
                                            gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment(0.0, 4.0),
                                              colors: [
                                                const Color(0xFF000000)
                                                    .withOpacity(0.4),
                                                const Color(0xFFFFFFFF)
                                                    .withOpacity(0.4),
                                              ],
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(45))),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )),
                        FlatButton(
                          onPressed: () {},
                          child: Text(
                            'Забыли пароль?',
                            style: whiteTextStyleSmall.copyWith(
                                color: colorPrimaryRed,
                                fontWeight: FontWeight.w600,
                                fontSize: 12.99),
                          ),
                        ),
                      ],
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
