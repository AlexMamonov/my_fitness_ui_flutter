import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/login_page/login_page.dart';
import 'package:my_fitness_ui_flutter/presentation/pages/main_page/main_page.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/theme.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '#MYFITNESS',
      theme: appLightTheme,
      initialRoute: 'login',
      routes: {
        'login': (context) => LoginPage(),
        '/': (context) => MainPage(),
      },
    );
  }
}
