import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final Widget notificationIcon = SvgPicture.asset(
    'assets/svg/icons/notification_icon.svg',
    height: 28,
    width: 28,
    color: Colors.black,
    semanticsLabel: 'notification_icon');

final Widget workoutNotificationIcon = SvgPicture.asset(
    'assets/svg/icons/workout_ntf_icon.svg',
    height: 24,
    width: 24,
    color: Colors.white,
    semanticsLabel: 'workout_ntf_icon');

final Widget workoutIcon = SvgPicture.asset('assets/svg/icons/workout_icon.svg',
    height: 20, width: 20, color: Colors.black, semanticsLabel: 'workout_icon');

final Widget scheduleNotificationIcon = SvgPicture.asset(
    'assets/svg/icons/schedule_ntf_icon.svg',
    height: 24,
    width: 24,
    color: Colors.white,
    semanticsLabel: 'schedule_ntf_icon');

final Widget scheduleIcon = SvgPicture.asset(
    'assets/svg/icons/schedule_icon.svg',
    height: 20,
    width: 20,
    color: Colors.black,
    semanticsLabel: 'schedule_icon');

final Widget selectedWorkoutIcon = SvgPicture.asset(
    'assets/svg/icons/selected_workout_icon.svg',
    height: 20,
    width: 20,
    color: Colors.black,
    semanticsLabel: 'selected_workout_icon');

final Widget selectedScheduleIcon = SvgPicture.asset(
    'assets/svg/icons/selected_schedule_icon.svg',
    height: 20,
    width: 20,
    semanticsLabel: 'selected_schedule_icon');

final Widget selectedHomeIcon = SvgPicture.asset(
    'assets/svg/icons/selected_home_icon.svg',
    height: 22,
    width: 22,
    semanticsLabel: 'selected_home_icon');

final Widget moreIcon = SvgPicture.asset('assets/svg/icons/more_icon.svg',
    height: 8, width: 15, color: Colors.black, semanticsLabel: 'more_icon');

final Widget homeIcon = SvgPicture.asset('assets/svg/icons/home_icon.svg',
    height: 22, width: 22, semanticsLabel: 'home_icon');

final Widget maleIcon = SvgPicture.asset(
  'assets/svg/icons/male_icon.svg',
  height: 30,
  width: 30,
  semanticsLabel: 'male_icon',
  color: Colors.white,
);

final Widget femaleIcon = SvgPicture.asset(
  'assets/svg/icons/female_icon.svg',
  height: 30,
  width: 30,
  semanticsLabel: 'female_icon',
  color: Colors.white,
);

final Widget snowflakeIcon = SvgPicture.asset(
  'assets/svg/icons/snowflake_icon.svg',
  height: 30,
  width: 30,
  semanticsLabel: 'snowflake_icon',
  color: Colors.white,
);
