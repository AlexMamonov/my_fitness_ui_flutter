import 'package:flutter/material.dart';
import 'colors.dart';

var appLightTheme = ThemeData(
    unselectedWidgetColor: Colors.white,
    primaryColor: colorPrimaryRed,
    textSelectionHandleColor: colorGray,
    fontFamily: 'OpenSans',
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor: Colors.transparent);
