import 'package:flutter/material.dart';
import 'colors.dart';

TextStyle mainTextStyle = TextStyle(
    fontSize: 27,
    color: Colors.black,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.italic);

TextStyle primaryTextStyleWhite = TextStyle(
    fontSize: 42,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontStyle: FontStyle.italic);

TextStyle grayColorTextStyle =
    TextStyle(fontSize: 14, color: colorGrayText, fontWeight: FontWeight.w600);

TextStyle grayLabelColorTextStyle =
    TextStyle(fontSize: 14, color: colorLabelGray, fontWeight: FontWeight.w600);

TextStyle selectedColorTextStyle = TextStyle(
  fontSize: 17,
  color: textColorBlack,
  fontWeight: FontWeight.w600,
);

TextStyle whiteTextStyleSmall =
    TextStyle(fontSize: 17, color: Colors.white, fontWeight: FontWeight.w400);

TextStyle menuTextStyleSmall =
    TextStyle(fontSize: 12, fontWeight: FontWeight.w500);

TextStyle lockerNameTextStyle = TextStyle(
    fontSize: 17,
    color: Colors.black,
    fontStyle: FontStyle.italic,
    fontWeight: FontWeight.bold);

TextStyle paymentWaitingTextStyle =
    TextStyle(fontSize: 17, color: colorOrange, fontWeight: FontWeight.w400);

TextStyle quarterFreezedTextStyle =
    TextStyle(fontSize: 14, color: colorLightBlue, fontWeight: FontWeight.w400);

TextStyle loginPageLabelStyle = TextStyle(
    fontSize: 17,
    color: colorLabelGray,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.5);
