import 'package:flutter/material.dart';
import 'package:my_fitness_ui_flutter/presentation/common_widgets/calendar_stip.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/colors.dart';
import 'package:my_fitness_ui_flutter/presentation/styleguide/text_style.dart';

List<String> monthLabels = [
  "январь",
  "февраль",
  "март",
  "апрель",
  "май",
  "июнь",
  "июль",
  "август",
  "сентябрь",
  "октябрь",
  "ноябрь",
  "декабрь"
];

List<String> dayLabels = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"];

onWeekSelect(data) {}
DateTime startDate = DateTime.now().subtract(Duration(days: 0));
DateTime endDate = DateTime.now().add(Duration(days: 3));
DateTime selectedDate = DateTime.now();

getMarkedIndicatorWidget() {
  return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
    Container(
      margin: EdgeInsets.only(left: 1, right: 1),
      width: 4,
      height: 4,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
    ),
    Container(
      margin: EdgeInsets.only(left: 1, right: 1),
      width: 4,
      height: 4,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
    ),
  ]);
}

dateTileBuilder(
    date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {
  bool isSelectedDate = date.compareTo(selectedDate) == 0;
  List<Widget> _children = [
    Text(dayName,
        style: isSelectedDate
            ? grayColorTextStyle.copyWith(fontSize: 13, color: Colors.white)
            : grayColorTextStyle.copyWith(fontSize: 13)),
    SizedBox(
      height: 10,
    ),
    Text(date.day.toString(),
        style: primaryTextStyleWhite.copyWith(fontSize: 27)),
  ];

  if (isDateMarked == true) {
    _children.add(getMarkedIndicatorWidget());
  }

  if (!isDateOutOfRange) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 150),
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 20, bottom: 20),
        decoration: BoxDecoration(
          color: !isSelectedDate ? Colors.transparent : colorDarkBlack,
          borderRadius: BorderRadius.all(Radius.circular(60)),
        ),
        child: Column(
          children: _children,
        ),
      ),
    );
  } else {
    return Container();
  }
}

monthLabelWidget(monthLabel) {
  return Container();
}

Widget buildCalendar({
  DateTime selectedDate,
  Function onDateSelected,
  List<DateTime> markedDates,
}) {
  return Container(
    width: 300,
    height: 130,
    child: CalendarStrip(
      selectedDate: selectedDate,
      startDate: startDate,
      endDate: endDate,
      onDateSelected: onDateSelected,
      onWeekSelected: onWeekSelect,
      dateTileBuilder: dateTileBuilder,
      iconColor: Colors.black87,
      monthNameWidget: monthLabelWidget,
      markedDates: markedDates,
      // containerDecoration: BoxDecoration(color: Colors.black12),
    ),
  );
}
